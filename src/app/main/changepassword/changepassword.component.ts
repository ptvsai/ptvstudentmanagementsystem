import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {

  constructor(private hc:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  changepass(p)
  {
  this.hc.put('/admin/changepassword',p).subscribe(res=>{
    if(res["message"]=="password changed")
    {
    alert(res["message"])
    this.router.navigate(["/main/login"])
    }
  })
  }
}
