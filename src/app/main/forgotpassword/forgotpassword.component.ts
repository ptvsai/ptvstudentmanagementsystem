import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  constructor(private service:HttpClient,private router:Router) { }

  ngOnInit() {
  }
forgot(pass)
{
  this.service.post("/admin/forgotpassword",pass).subscribe((res)=>{
    if(res["message"]=="user found")
    {
    alert(res["message"])
    this.router.navigate(["/main/otp"])
    }
    else{
      alert(res["message"])
      this.router.navigate(["/main/forgotpassword"])
    }
  })
}
}