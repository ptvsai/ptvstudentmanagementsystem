import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  constructor(private hc:HttpClient,private router:Router) { }

  ngOnInit() {
  }
otp(o)
{
this.hc.post('/admin/verifyotp',o).subscribe(res=>{
  if(res["message"]=="verifiedOTP")
    {
    alert(res["message"])
    this.router.navigate(["/main/changepassword"])
    }
    else{
      alert(res["message"])
      this.router.navigate(["/main/otp"])
    }
  })
}
}
