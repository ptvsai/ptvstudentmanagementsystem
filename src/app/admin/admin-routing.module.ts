import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { UpdatemarksComponent } from './adminlogin/updatemarks/updatemarks.component';
import { UpdateattendanceComponent } from './adminlogin/updateattendance/updateattendance.component';
import { UpdatenotificationsComponent } from './adminlogin/updatenotifications/updatenotifications.component';
import { UpdatefeeComponent } from './adminlogin/updatefee/updatefee.component';
import { Logout1Component } from './adminlogin/logout1/logout1.component';
import { RequestslistComponent } from './adminlogin/requestslist/requestslist.component';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';

const routes: Routes = [
  {path:"",redirectTo:"adminlogin/studentprofile",pathMatch:"full"} ,
  {
    path:'adminlogin',
    component:AdminloginComponent
    ,
 children:[   
  {
    path:'addstudent',
    component:AddstudentComponent
  },
  {
    path:'studentprofile',
    component:StudentprofileComponent
  }, 
  {
      path:'marks',
      component:UpdatemarksComponent
    },
    {
      path:'attendance',
      component:UpdateattendanceComponent
    },
    {
      path:'feeStatus',
      component:UpdatefeeComponent
    },
    {
      path:'notifications',
      component:UpdatenotificationsComponent
    },
    {
      path:'logout',
      component:Logout1Component
    },
    {
      path:'Request list',
      component:RequestslistComponent
    },]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
