import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updatenotifications',
  templateUrl: './updatenotifications.component.html',
  styleUrls: ['./updatenotifications.component.css']
})
export class UpdatenotificationsComponent implements OnInit {
noti:any;
  constructor(private service4:DatatransferService,private httPclient:HttpClient,private router:Router) { }

  ngOnInit() {
  }
  sendto3(v)
  {
    if(v.notification=="")
    {
      alert(["enter the data in the fields"])
    }
      else
   {
    this.noti=v;
    this.httPclient.post("/admin/savenotify",v).subscribe((res)=>
    {
      alert(res['message'])
      this.router.navigate(["/adminlogin/studentprofile"]);
    }
    )
  }

}
}
