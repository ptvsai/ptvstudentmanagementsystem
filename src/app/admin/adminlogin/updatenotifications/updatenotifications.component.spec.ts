import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdatenotificationsComponent } from './updatenotifications.component';

describe('UpdatenotificationsComponent', () => {
  let component: UpdatenotificationsComponent;
  let fixture: ComponentFixture<UpdatenotificationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdatenotificationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdatenotificationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
