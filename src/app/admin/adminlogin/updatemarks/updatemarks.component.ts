import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updatemarks',
  templateUrl: './updatemarks.component.html',
  styleUrls: ['./updatemarks.component.css']
})
export class UpdatemarksComponent implements OnInit {
marks:any[]=[];
searchterm:any[]
  constructor(private service:DatatransferService,private httpclient:HttpClient,private router:Router) { }

  ngOnInit() {
    this.service.readData3().subscribe(marks=>
      {
        if(marks["message"]=="unauthorized access")
      {
        alert(["unauthorized access"])
        this.router.navigate(["/main/login"])
      }
        this.marks=marks['message']
      })
  }
  
sendData(x)
  {
    if(x.rollnumber=="" || x.subject=="" || x.marks=="" || x.total=="")
    {
      alert(["enter the data in the fields"])
    }
      else
   {
    this.httpclient.post("/admin/savemr",x).subscribe((res)=>
{
  alert(res['message'])
}
)
}
this.service.readData3().subscribe(marks=>
  {
    this.marks=marks['message']
  })
  }
  deleteRecord(rollnumber)
  {
    this.httpclient.delete(`/admin/deletemr/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.marks=res['data']
    })
  }
  
  b:boolean=false;
  objectToModify:object;
  editRecord(obj1)
  {
    this.objectToModify=obj1;
    this.b=true
  }
  onSubmit(modifiedobject)
  {
    this.b=false
    this.httpclient.put('/admin/update',modifiedobject).subscribe((res)=>{
      alert(res['message'])
    })
  }

}
