import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updatefee',
  templateUrl: './updatefee.component.html',
  styleUrls: ['./updatefee.component.css']
})
export class UpdatefeeComponent implements OnInit {
t:any[]=[];
searchterm:any[]
  constructor(private service3:DatatransferService,private htTpclient:HttpClient,private router:Router) { }

  ngOnInit() {
    this.service3.readData2().subscribe(fee=>
      {
         if(fee["message"]=="unauthorized access")
        {
          alert(["unauthorized access"])
          this.router.navigate(["/main/login"])
        }
        this.t=fee['message']
      })
  }
  sendto2(d6)
  {
      if(d6.rollnumber=="" || d6.totalfee=="" || d6.paidfee=="" || d6.balancefee=="")
      {
        alert(["enter the data in the fields"])
      }
        else
     {

  this.htTpclient.post("/admin/savefee",d6).subscribe((res)=>
{
  alert(res['message'])
  this.service3.readData2().subscribe(fee=>
    {
      this.t=fee['message']
    })
}
)

  }
}
  c:boolean=false;
  objectToModify2:object;
  editRecord(obj2)
  {
    this.objectToModify2=obj2;
    this.c=true
  }
  onSubmit2(modifiedobject2)
  {
    this.c=false
    this. htTpclient.put('/admin/updatefee',modifiedobject2).subscribe((res)=>{
      alert(res['message'])
    })
  }
  deleteRecord2(rollnumber)
  {
    this.htTpclient.delete(`/admin/deletefee/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.t=res['data']
    })
  }
}
