import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-studentprofile',
  templateUrl: './studentprofile.component.html',
  styleUrls: ['./studentprofile.component.css']
})
export class StudentprofileComponent implements OnInit {
data:any[];
searchterm:any[]
  constructor(private ds8:DatatransferService,private httpClient:HttpClient, private router:Router) { }

  ngOnInit() {
    this.ds8.readData().subscribe(data=>
    {
      if(data["message"]=="unauthorized access")
      {
        alert(data["message"])
        this.router.navigate(["/main/login"])
      }
      else
      {
      this.data=data['data']
      }
    })
  
  }
  deleteRecord3(rollnumber)
  {
    this.httpClient.delete(`/admin/deletepro/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.data=res['data']
    })
  }

}
