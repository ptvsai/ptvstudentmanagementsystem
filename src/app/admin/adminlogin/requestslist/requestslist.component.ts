import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-requestslist',
  templateUrl: './requestslist.component.html',
  styleUrls: ['./requestslist.component.css']
})
export class RequestslistComponent implements OnInit {


loggedUser;
data:any[]=[]
  constructor(private ds5:DatatransferService, private http:HttpClient,private router:Router) { }

  ngOnInit() {
  this.http.get('/admin/readreq').subscribe(data=>{
    if(data["message"]=="unauthorized access")
        {
          alert(["unauthorized access"])
          this.router.navigate(["/main/login"])
        }
    this.data=data['message']
        
  })

  }
  
  send2(rollnumber)
  {
   this.loggedUser=this.ds5.sendloggeduser()
   this.http.post('/admin/saveres',({"message":"request is accepted","rollnumber":rollnumber})).subscribe((res)=>{
     alert(res["message"])
     this.http.get('/admin/readreq').subscribe(data=>{
      this.data=data['message']
    })
   })
 
  }
  v:boolean=false
  reject(r,rollnumber)
  {
    this.v=false
    r.rollnumber=rollnumber
    r.message="Request is Rejected"
   this.http.post('/admin/saveres',r).subscribe((res)=>{
     alert(res["message"])
     this.http.get('/admin/readreq').subscribe(data=>{
      this.data=data['message']
      console.log(this.data)
    })
   })
 
  }
 
  send3()
  {
    this.v=true
 
  }
}
