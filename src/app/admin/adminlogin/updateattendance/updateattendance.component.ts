import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-updateattendance',
  templateUrl: './updateattendance.component.html',
  styleUrls: ['./updateattendance.component.css']
})
export class UpdateattendanceComponent implements OnInit {
 h:any[]=[];
 searchterm:any[]
  constructor(private service1:DatatransferService,private hTtpclient:HttpClient,private router:Router) { }

  ngOnInit() {
    this.service1.readData1().subscribe(attendance=>
      {
          if(attendance["message"]=="unauthorized access")
        {
          alert(["unauthorized access"])
          this.router.navigate(["/main/login"])
        }
        this.h=attendance['message']
      })
  }
  sendto1(d)
  {
    if(d.rollnumber=="" || d.monthattendance=="" || d.overallattendance=="")
    {
      alert(["enter the data in the fields"])
    }
      else
      {
   this.h.push(d)
this.hTtpclient.post("/admin/saveatten",d).subscribe((res)=>
{
  alert(res['message'])
  this.service1.readData1().subscribe(attendance=>
    {
      this.h=attendance['message']
    })
}
)

  }
}
  
  d:boolean=false;
  objectToModify1:object;
  editRecord(obj1)
  {
    this.objectToModify1=obj1;
    this.d=true
  }
  onSubmit1(modifiedobject1)
  {
   
    this.d=false
    this. hTtpclient.put('/admin/updateatt',modifiedobject1).subscribe((res)=>{
      alert(res['message'])
    })
  }


  deleteRecord1(rollnumber)
  {
    this.hTtpclient.delete(`/admin/deleteatt/${rollnumber}`).subscribe((res)=>{
      alert(res['message'])
      this.h=res['data']
    })
  }
}
