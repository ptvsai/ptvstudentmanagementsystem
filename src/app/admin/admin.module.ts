import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminloginComponent } from './adminlogin/adminlogin.component';
import { UpdatemarksComponent } from './adminlogin/updatemarks/updatemarks.component';
import { UpdateattendanceComponent } from './adminlogin/updateattendance/updateattendance.component';
import { UpdatefeeComponent } from './adminlogin/updatefee/updatefee.component';
import { UpdatenotificationsComponent } from './adminlogin/updatenotifications/updatenotifications.component';
import { Logout1Component } from './adminlogin/logout1/logout1.component';
import { FormsModule } from '@angular/forms';
import { RequestslistComponent } from './adminlogin/requestslist/requestslist.component';
import { AddstudentComponent } from './adminlogin/addstudent/addstudent.component';
import { StudentprofileComponent } from './adminlogin/studentprofile/studentprofile.component';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [AdminloginComponent, UpdatemarksComponent, UpdateattendanceComponent, UpdatefeeComponent, UpdatenotificationsComponent, Logout1Component, RequestslistComponent, AddstudentComponent, StudentprofileComponent, SearchPipe],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule
  ]
})
export class AdminModule { }
