import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(student: any, searchterm: any): any {
    if(!searchterm)
   {
     return student
   }
   {
     return student.filter(student=>student.rollnumber.toLowerCase().indexOf(searchterm.toLowerCase())!==-1)
   }
  }
  
}
