import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class DatatransferService {

user:any


  constructor(private hc:HttpClient) { }
  readData():Observable<any[]>
  {
    return this.hc.get<any[]>('admin/readstd')
  }
  readData1():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readatt')
  }
  readData2():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readfee')
  }
  readData3():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readmr')
  }
  readData4():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readnotify')
  }
  readData5():Observable<any[]>
  {
    return this.hc.get<any[]>('student/readreq')
  }
  loggeduser(user)
  {
    this.user=user[0]
  }
  sendloggeduser()
  {
  return this.user
  }
  viewsepcification(user)
  {
   return this.hc.post<any[]>('student/viewspecificmarks',user)
  }
  viewsepcification1(user)
  {
    return this.hc.post<any[]>('student/viewspecificatt',user)
  }
  viewsepcification2(user)
  {
    return this.hc.post<any[]>('student/viewspecificfee',user)
  }
  viewsepcification3(user)
  {
    return this.hc.post<any[]>('student/viewspecificstd',user)
  }
  viewsepcification4(user)
  {
    return this.hc.post<any[]>('student/viewspecificreq',user)
  }

}
