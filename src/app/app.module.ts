import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './main/home/home.component';
import { LoginComponent } from './main/login/login.component';
import { AdminModule } from './admin/admin.module';
import { StudentModule } from './student/student.module';
import {FormsModule}      from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS}       from  '@angular/common/http'
import { AuthorizationService } from './authorization.service';
import { ForgotpasswordComponent } from './main/forgotpassword/forgotpassword.component';
import { OtpComponent } from './main/otp/otp.component';
import { ChangepasswordComponent } from './main/changepassword/changepassword.component';


@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    HomeComponent,
    LoginComponent,
    ForgotpasswordComponent,
    OtpComponent,
    ChangepasswordComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    AdminModule,
    StudentModule,
    HttpClientModule
  ],
  providers: [
    {
      provide:HTTP_INTERCEPTORS,
      useClass:AuthorizationService,
      multi:true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
