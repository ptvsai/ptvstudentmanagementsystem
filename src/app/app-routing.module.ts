import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main/main.component';
import { HomeComponent } from './main/home/home.component';
import { LoginComponent } from './main/login/login.component';
import { ForgotpasswordComponent } from './main/forgotpassword/forgotpassword.component';
import { OtpComponent } from './main/otp/otp.component';
import { ChangepasswordComponent } from './main/changepassword/changepassword.component';

const routes: Routes = [
  { path:'main',
  component:MainComponent,
children:[
{
path:'home',
component:HomeComponent
},{
path:'login',
component:LoginComponent
},
{
path:'forgotpassword',
component:ForgotpasswordComponent
},
{
  path:'otp',
  component:OtpComponent
  },
  {
    path:'changepassword',
    component:ChangepasswordComponent
    },

]},
{path:"admin",loadChildren:()=>import('./admin/admin.module').then(mod=>mod.AdminModule)},
{path:"student",loadChildren:()=>import('./student/student.module').then(mod=>mod.StudentModule)},
{path:"",redirectTo:"main/home",pathMatch:"full"} 
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
