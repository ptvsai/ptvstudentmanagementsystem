import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './studentlogin/profile/profile.component';
import { MarksComponent } from './studentlogin/marks/marks.component';
import { AttendanceComponent } from './studentlogin/attendance/attendance.component';
import { FeestatusComponent } from './studentlogin/feestatus/feestatus.component';
import { Logout2Component } from './studentlogin/logout2/logout2.component';

import { StudentloginComponent } from './studentlogin/studentlogin.component';
import { NotificationsComponent } from './studentlogin/notifications/notifications.component';
import { RequestComponent } from './studentlogin/request/request.component';

const routes: Routes = [
  {path:"",redirectTo:"studentlogin/profile",pathMatch:"full"} ,
  {
    path:'studentlogin',
    component:StudentloginComponent,
   children: [
  {
    path:'profile',
    component:ProfileComponent
},{ 
    path:'Marks',
    component:MarksComponent
},
{
    path:'Attendance',
    component:AttendanceComponent
},
{
    path:'Feestatus',
    component:FeestatusComponent
},
{
    path:'Notification',
    component:NotificationsComponent
},
{
  path:'Request',
  component:RequestComponent
},
{
    path:'logout2',
    component:Logout2Component
}]
  }];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
