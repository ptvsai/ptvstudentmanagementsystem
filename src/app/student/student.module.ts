import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { StudentloginComponent } from './studentlogin/studentlogin.component';
import { MarksComponent } from './studentlogin/marks/marks.component';
import { AttendanceComponent } from './studentlogin/attendance/attendance.component';
import { FeestatusComponent } from './studentlogin/feestatus/feestatus.component';
import { NotificationsComponent } from './studentlogin/notifications/notifications.component';
import { Logout2Component } from './studentlogin/logout2/logout2.component';
import { ProfileComponent } from './studentlogin/profile/profile.component';
import { RequestComponent } from './studentlogin/request/request.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [StudentloginComponent, MarksComponent, AttendanceComponent, FeestatusComponent, NotificationsComponent, Logout2Component, ProfileComponent, RequestComponent],
  imports: [
    CommonModule,
    StudentRoutingModule,
    FormsModule
  ]
})
export class StudentModule { }
