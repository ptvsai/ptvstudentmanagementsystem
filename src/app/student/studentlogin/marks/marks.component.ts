import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-marks',
  templateUrl: './marks.component.html',
  styleUrls: ['./marks.component.css']
})
export class MarksComponent implements OnInit {
marks:any[]=[];
user;
  constructor(private std:DatatransferService,private router:Router) { }

  ngOnInit() {
    this.user=this.std.sendloggeduser()
    this.std.viewsepcification(this.user).subscribe(marks=>
      {
        if(marks["message"]=="unauthorized access")
        {
          alert(["unauthorized access"])
          this.router.navigate(["/main/login"])
        }
        else
        this.marks=marks['message']
      })
  }
  }


