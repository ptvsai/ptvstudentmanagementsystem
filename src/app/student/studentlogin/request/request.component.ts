import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  b:boolean=true;
  accept:any[];
  loggedUser;
  user
  constructor(private service4:DatatransferService,private http:HttpClient, private router:Router) { }

  ngOnInit() {
  this.user=this.service4.sendloggeduser()
  this.service4.viewsepcification4(this.user).subscribe(accept=>{
    if(accept["message"]=="unauthorized access")
      {
        alert(["unauthorized access"])
        this.router.navigate(["/main/login"])
      }
      else
    this.accept=accept['message'];
  
  })
  }
  send1(d6)
  {
    this.loggedUser=this.service4.sendloggeduser();
    d6.rollnumber=this.loggedUser.rollnumber;
    this.http.post('/student/savereq',d6).subscribe(res=>{
      alert(res['message'])
    })
    this.b=false
  }

}
