import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-feestatus',
  templateUrl: './feestatus.component.html',
  styleUrls: ['./feestatus.component.css']
})
export class FeestatusComponent implements OnInit {
fee:any[]=[];
user;
  constructor(private ds3:DatatransferService,private router:Router) { }

  ngOnInit() {
    this.user=this.ds3.sendloggeduser()
    this.ds3.viewsepcification2(this.user).subscribe(fee=>
      {
         if(fee["message"]=="unauthorized access")
      {
        alert(["unauthorized access"])
        this.router.navigate(["/main/login"])
      }
      else
        this.fee=fee['message']
      })
  }

}
