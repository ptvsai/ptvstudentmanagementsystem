import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-logout2',
  templateUrl: './logout2.component.html',
  styleUrls: ['./logout2.component.css']
})
export class Logout2Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
    localStorage.clear()
    this.router.navigate(['/main/login'])
  }

}
