import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
notif:any[]=[];
  constructor(private ds4:DatatransferService, private router:Router) { }

  ngOnInit() {
  
   this.ds4.readData4().subscribe(notif=>
     {
      if(notif["message"]=="unauthorized access")
      {
        alert(["unauthorized access"])
        this.router.navigate(["/main/login"])
      }
      else
       this.notif=notif['message']
     })
  }

}
