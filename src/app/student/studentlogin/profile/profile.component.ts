import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
data:any[]=[]
user;
  constructor(private ds8:DatatransferService, private router:Router) { }

  ngOnInit() {
    this.user=this.ds8.sendloggeduser()
    this.ds8.viewsepcification3(this.user).subscribe(data=>
      {
        if(data['message']=="unauthorized access")
        {
          alert(["unauthorized access"])
          this.router.navigate(["/main/login"])
        }
        else{
        this.data=data['message']
        }
      })
  }

}
