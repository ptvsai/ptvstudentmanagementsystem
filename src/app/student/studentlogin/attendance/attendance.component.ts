import { Component, OnInit } from '@angular/core';
import { DatatransferService } from 'src/app/datatransfer.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.css']
})
export class AttendanceComponent implements OnInit {
attendance:any[]=[];
user;
  constructor(private ds2:DatatransferService,private router:Router) { }

  ngOnInit() {
    this.user=this.ds2.sendloggeduser()
    this.ds2.viewsepcification1(this.user).subscribe(attendance=>
      {
        if(attendance["message"]=="unauthorized access")
      {
        alert(["unauthorized access"])
        this.router.navigate(["/main/login"])
      }
      else
        this.attendance=attendance['message']
      })
  }

}
