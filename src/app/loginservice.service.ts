import { Injectable, ɵConsole } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginserviceService {

  constructor(private hc:HttpClient) { }
  dologin(userObject):Observable<any>
  {
    return this.hc.post<any>('admin/saveli',userObject)
  }
  dologinadmin(userObject):Observable<any>
  {
    return this.hc.post<any>('admin/adminlogin',userObject)
  }
}
