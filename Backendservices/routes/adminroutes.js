const exp=require("express")
var adminroutes=exp.Router()
const initDb=require('../db.config').initDb
const getDb=require('../db.config').getDb
const bodyparser=require('body-parser')
const bcrypt=require('bcrypt')
const jwt=require('jsonwebtoken')
const secretkey="xxxxxx"
const authorization=require("../middleware/authorization")
const nodemailer=require("nodemailer")
const accountSid = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
const authToken = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
const client = require('twilio')(accountSid, authToken);
adminroutes.use(bodyparser.json())
initDb()
adminroutes.post('/save',authorization,(req,res,next)=>{
    
    var transporter = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 465,
        secure: true,
        auth: {
            user: 'xxxxxxx@gmail.com', // Your email id
            pass: 'xxxxxxxxxxxxx' // Your password
        },
        tls: {
            // do not fail on invalid certs
            rejectUnauthorized: false
        }
    });
        let info= transporter.sendMail({
            from:'"admin"<xxxxxxxx@gmail.com>',
            to:req.body.gmail,
            subject:"student credentials",
            text:`rollnumber: ${req.body.rollnumber},password: ${req.body.password}`
        });
   
    dbo=getDb()
  dbo.collection("stdproCollection").find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,studentArray)=>{
   if(studentArray=="")                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
   {

    bcrypt.hash(req.body.password,6,(err,hashedpassword)=>
    {
        if(err)
        {
            next(err)
        }
        else
        {
            req.body.password=hashedpassword
            dbo.collection("stdproCollection").insertOne(req.body,(err,success)=>{
                if(err)
                {
                    next(err)
                }
                else{
                    res.json({message:"success"})
                }
            })
        }
    })
   }
   else{
       res.json({message:"user already exist"})
   }  

})
})

adminroutes.post('/savemr',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdmrCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/saveatten',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdattCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/savefee',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdfeeCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/savenotify',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdnotiCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:"successfully inserted"})
            }
        }
        )
    }
});
adminroutes.post('/saveres',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdresCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                
                dbo.collection('stdreqCollection').deleteOne({rollnumber:{$eq:req.body.rollnumber}},(err,dataArray)=>{
                    if(err)
                    {
                        next(err)
                    }
                    else{
                        res.json({message:"Reply sent to student",data:dataArray})
                    
                    }

                })
            }
        }
        )
    }
});


adminroutes.get('/readreq',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdreqCollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
         
          
        }
    })
});
adminroutes.get('/readstd',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdproCollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:"success",data:dataArray})
        }
    })
});
adminroutes.post('/saveli',(req,res,next)=>{
    dbo=getDb();
    dbo.collection('stdproCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,userArray)=>{
        if(userArray.length===0)
        {
            res.json({message:"Invalid student"})
        }
      else{
          bcrypt.compare(req.body.password,userArray[0].password,(err,result)=>{
            if(result==true)
            {
                const signedtoken=jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:'7d'})
                res.json({message:"studentlogin success",token:signedtoken,data:userArray})
            }
        else
        {
            res.json({message:"Invalid password"})
        }
    })
    }
    })
});
adminroutes.post('/adminlogin',(req,res,next)=>{
    dbo=getDb()
    dbo.collection('adminlogin').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,userArray)=>{
        if(userArray.length==0)
        {
            res.json({message:"Invalid admin"})
        }
        else if(userArray[0].password!==req.body.password)
        {
            res.json({message:"Invalid admin password"})
        }
        else{
            const signedtoken=jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:'7d'})
            res.json({message:"Adminlogin success",token:signedtoken})
        }
    })
})
adminroutes.post('/forgotpassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    dbo.collection("stdproCollection").find({rollnumber:req.body.rollnumber}).toArray((err,userArray)=>{
        if(err){
            next(err)
        }
        else{
            if(userArray.length===0){
                res.json({message:"user not found"})
            }
            else{

                jwt.sign({rollnumber:userArray[0].rollnumber},secretkey,{expiresIn:3600},(err,token)=>{
                    if(err){
                     next(err);
                    }
                    else{
                        var OTP=Math.floor(Math.random()*99999)+11111;
                        console.log(OTP)
                         client.messages.create({
                            body: OTP,
                            from: '+xxxxxxxx', // From a valid Twilio number
                            to: "+91"+userArray[0].phonenumber,  // Text this number
                        })
                        .then((message) => {
                            dbo.collection('OTPCollection').insertOne({
                                OTP:OTP,
                                rollnumber:userArray[0].rollnumber,
                                OTPGeneratedTime:new Date().getTime()+15000
                        },(err,success)=>{
                            if(err){
                                next(err)
                            }
                            else{                                        
                                res.json({"message":"user found",
                                    "token":token,
                                    "OTP":OTP,
                                    "rollnumber":userArray[0].rollnumber
                                })
                            }
                        })
                        });

                    }
                    
                })
            }
        }
    })
})

//verify OTP
adminroutes.post('/verifyotp',(req,res,next)=>{
    console.log(req.body)
    console.log(new Date().getTime())
    var currentTime=new Date().getTime()
    dbo.collection('OTPCollection').find({"OTP":req.body.OTP}).toArray((err,OTPArray)=>{
        if(err){
            next(err)
        }
        else if(OTPArray.length===0){
            res.json({"message":"invalidOTP"})
        }
        else if(OTPArray[0].OTPGeneratedTime < req.body.currentTime){
            res.json({"message":"invalidOTP"})
        }
        else{
            
            dbo.collection('OTPCollection').deleteOne({OTP:req.body.OTP},(err,success)=>{
                if(err){
                    next(err);
                }
                else{
                    console.log(OTPArray)
                    res.json({"message":"verifiedOTP"})
                }
            })
        }
    })
});
//changing password
adminroutes.put('/changepassword',(req,res,next)=>{
    console.log(req.body)
    dbo=getDb()
    bcrypt.hash(req.body.password,6,(err,hashedPassword)=>{
        if (err) {
            next(err)
        } else {
            console.log(hashedPassword)
           dbo.collection("stdproCollection").updateOne({rollnumber:req.body.rollnumber},{$set:{
                password:hashedPassword
            }},(err,success)=>{
                if(err){
                    next(err)
                }
                else{
                    res.json({"message":"password changed"})
                }
            }) 
        }
    })
    
});

adminroutes.delete('/deletepro/:rollnumber',authorization,(req,res,next)=>{
dbo=getDb()
dbo.collection("stdproCollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
       next(err)
    }
    else
    {
        dbo.collection('stdproCollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                next(err)
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deletemr/:rollnumber',authorization,(req,res,next)=>{
dbo=getDb()
dbo.collection("stdmrCollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('stdmrCollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deleteatt/:rollnumber',authorization,(req,res,next)=>{
dbo=getDb()
dbo.collection("stdattCollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('stdattCollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

});
adminroutes.delete('/deletefee/:rollnumber',authorization,(req,res,next)=>{
dbo=getDb()
dbo.collection("stdfeeCollection").deleteOne({rollnumber:{$eq:req.params.rollnumber}},(err,success)=>{
    if(err)
    {
        next(err)
    }
    else
    {
        dbo.collection('stdfeeCollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                console.log("error")
            }
       
    
    else{
        res.json({message:"Record Deleted",data:dataArray})

    }
})
    }
})

})
adminroutes.put('/update',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdmrCollection').update({rollnumber:{$eq:req.body.rollnumber}},{$set
        :{rollnumber:req.body.rollnumber,subject:req.body.subject,marks:req.body.marks,total:req.body.total}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.put('/updateatt',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdattCollection').update({rollnumber:{$eq:req.body.rollnumber}},{$set:{rollnumber:req.body.rollnumber,monthattendance:req.body.monthattendance,overallattendance:req.body.overallattendance}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
});
adminroutes.put('/updatefee',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdfeeCollection').update({rollnumber:{$eq:req.body.rollnumber}},
        {$set:{rollnumber:req.body.rollnumber,totalfee:req.body.totalfee,paidfee:req.body.paidfee,balancefee:req.body.balancefee}},(err,success)=>{
        if(err){
            next(err)
        }
        else{
            res.json({message:"success"})
        }
    })
})

adminroutes.use((err,req,res,next)=>{
    console.log(err)
})
module.exports=adminroutes