const exp=require("express")
var studentroutes=exp.Router()
const initDb=require('../db.config').initDb
const getDb=require('../db.config').getDb
const bodyparser=require('body-parser')
const authorization=require("../middleware/authorization")
studentroutes.use(bodyparser.json())
initDb()
studentroutes.get('/readatt',authorization,(req,res,next)=>{
    dbo=getDb()
    
        dbo.collection('stdattCollection').find().toArray((err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
                res.json({message:dataArray})
            }
        }
        )
});
studentroutes.get('/readfee',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdfeeCollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});
studentroutes.get('/readmr',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdmrCollection').find().toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             res.json({message:dataArray})
         }
    })
});
studentroutes.get('/readnotify',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdnotiCollection').find().toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
         
          
        }
    })
});

studentroutes.post('/savereq',authorization,(req,res,next)=>{
    dbo=getDb()
    if(req.body=={})
    {
        res.json({message:"server didnot receive data"})
    }
    else
    {
        dbo.collection("stdreqCollection").insertOne(req.body,(err,dataArray)=>{
            if(err)
            {
                next(err)
            }
            else{
          
              dbo.collection('stdresCollection').deleteOne({rollnumber:{$eq:req.body.rollnumber}},(err,dataArray)=>{
                  if(err)
                  {
                      next(err)
                  }
                  else{
                        res.json({message:"Request sent to admin"})
                  }
              })
            }
        }
        )
    }
});
studentroutes.post('/viewspecificmarks',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdmrCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             res.json({message:dataArray})
         }
    })
});
studentroutes.post('/viewspecificatt',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdattCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             res.json({message:dataArray})
         }
    })
});
studentroutes.post('/viewspecificfee',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdfeeCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>
    {
        if(err)
        {
            next(err)
        }
         else
         {
             res.json({message:dataArray})
         }
    })
});
studentroutes.post('/viewspecificstd',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdproCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});
studentroutes.post('/viewspecificreq',authorization,(req,res,next)=>{
    dbo=getDb()
    dbo.collection('stdresCollection').find({rollnumber:{$eq:req.body.rollnumber}}).toArray((err,dataArray)=>{
        if(err)
        {
            next(err)
        }
        else{
            res.json({message:dataArray})
        }
    })
});
studentroutes.use((err,req,res,next)=>{
    console.log(err)
})
module.exports=studentroutes