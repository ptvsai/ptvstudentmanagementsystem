//install and import express
const exp=require("express")
app=exp()

//install and import path module
const path=require("path")
console.log(__dirname);
//connecting angular app with server
app.use(exp.static(path.join(__dirname,'../dist/PROJECT-REAL/')));
//routing the user and admin
const adminroutes=require('./routes/adminroutes');
const studentroutes=require('./routes/studentroutes');
app.use('/admin',adminroutes)
app.use('/student',studentroutes)

//port number
app.listen(process.env.PORT || 8080,()=>{
    console.log('server started')
})